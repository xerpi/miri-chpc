\documentclass[12pt,a4paper,oneside,hidelinks]{beamer}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{lmodern}
\usepackage{csquotes}
\usepackage{listings}
\usepackage[font=scriptsize]{caption}
\usepackage[numbers]{natbib}
\usepackage{textcomp}

\usetheme[progressbar=frametitle]{metropolis}
\captionsetup[figure]{labelformat=empty}

\title{Acyclic code scheduling}
\subtitle{\small{Compilers for High Performance Computers}}
\author{Sergi Granell}
\institute{Universitat Politècnica de Catalunya}
\date{December, 2018}

\makeatletter
\setbeamertemplate{headline}{%
  \begin{beamercolorbox}[colsep=1.5pt]{upper separation line head}
  \end{beamercolorbox}
  \begin{beamercolorbox}{section in head/foot}
    \vskip2pt \insertnavigation{\paperwidth} \vskip2pt
  \end{beamercolorbox}%
  \begin{beamercolorbox}[colsep=1.5pt]{lower separation line head}
  \end{beamercolorbox}
}
\makeatother

\setbeamercolor{section in head/foot}{fg=normal text.bg, bg=structure.fg}


\begin{document}

\begin{frame}[plain,noframenumbering]
	\titlepage
\end{frame}

\begin{frame}{Outline}
\begin{enumerate}
\item Basic concepts
\item Phase ordering between Register allocation and Code scheduling
\item One-register machines
\item Binate covering for scheduling one-register machines
\end{enumerate}
\end{frame}

\section{Basic concepts}

\begin{frame}{Basic concepts}
\begin{itemize}
\item ILP: processor/compiler techniques
\item Code-scheduling constraints (control, data, resources)
\item Basic-block scheduling
	\begin{itemize}
	\item List scheduling: topological sort, ready list, schedule cycle-by-cycle
	\end{itemize}
\item Global code Scheduling
	\begin{itemize}
	\item BBs are small $\Rightarrow$ schedule group  of BBs
	\item DAG of BBs: control flow graph (CFG)
	\item Traces, superblocks, hyperblocks, treegions...
	\end{itemize}
\end{itemize}
\end{frame}

\section{Register Allocation and Code Scheduling ordering}

\begin{frame}{Register Allocation and Code Scheduling ordering}
\begin{itemize}
\item RA and CS are last steps of compilation (code generation)
\item RA first: constrains CS (new dependencies)
\item CS first: increase register pressure (spilling)
\item Depending on processor execute one before other
\item Not easy to choose which one goes first
	\begin{itemize}
	\item[$\Rightarrow$] \alert{perform CS before and after RA}
	\end{itemize}
\end{itemize}
\end{frame}


\section{One-register machines}

\begin{frame}{One-register machines}
\begin{itemize}
\item One register: \alert{accumulator}
\item \textbf{acc} \textleftarrow \ \textit{op} \textbf{acc}\hspace*{\fill}(unary operator)
\item \textbf{acc} \textleftarrow \ \textbf{acc} \textit{op} \textbf{mem}\hspace*{\fill}(memory-right binary operator)
\item \textbf{acc} \textleftarrow \ \textbf{mem} \textit{op} \textbf{acc}\hspace*{\fill}(memory-left binary operator)
\item \textbf{acc} \textleftarrow \ \textbf{mem}\hspace*{\fill}(reload)
\item \textbf{mem} \textleftarrow \ \textbf{acc}\hspace*{\fill}(spill)
\end{itemize}
\end{frame}

\section{Binate covering}

\begin{frame}{Binate covering problem}
\begin{itemize}
\item Finding a minimum cost assignment to variables that is a solution of a Boolean equation $f = 1$
	\begin{itemize}
	\item Defined as $\langle X, Y, cost \rangle$
	\item $X$ is a set of Boolean variables
	\item $Y$ is a set of clauses
	\item $cost$ maps $x_i \in X$ to positive integers
	\item Each $y_j \in Y$ is a disjunction of variables of $X$.
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Worms and worm-graphs (1)}
\begin{itemize}
\item Subject DAG = DAG without inputs/outputs vertices

\item DAG can be \textit{d-cyclic}/\textit{d-acyclic} and \textit{u-cyclic}/\textit{u-acyclic}

\item Subject DAG $D \langle V, E \rangle$. A \textit{worm} $w$ in $D$ is a subset of $D$ vertices that appear consecutively in the schedule

\item \textit{Worm-partition}: set of disjoint worms

\item \textit{Worm-graph} $G$ of $D$: directed multigraph with each worm-partition collapsed into single vertex

\item Every d-acyclic DAG has a topological sort, a \alert{\textit{sufficient}} (not \textit{necessary}) condition for a worm-graph to be \textit{legal} is \alert{$G$ to be d-acyclic}
\end{itemize}
\end{frame}

\begin{frame}{Worms and worm-graphs (2)}
\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{figures/worm_wormgraph.png}
\caption{\citet{Liao:1998:NVC:270580.270583}}
\end{figure}
\end{frame}

\begin{frame}{Binate Covering Formulation}
\begin{itemize}
\item Map the binate covering problem into one-register machines with Boolean variables:
	\begin{enumerate}
	\item \textit{adjacency variables} associated with each edge $e_i$
	\item matches $m$ of patters of instructions at vertex $v$
	\item spills and reloads
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}{Fundamental Adjacency Clauses}
\begin{itemize}
\item If $v$ has multiple fanouts, select at most one
\item If $v$ has multiple fanins, select at most one
\item Those constraints are:
	\begin{itemize}
	\item \alert{necessary} for a worm-graph to be legal in any DAG
	\item \alert{sufficient} if the subject DAG is u-acyclic
	\end{itemize}
\item Clauses:
	\begin{itemize}
	\item $\overline{e_i} + \overline{e_j}$ for every pair of fanin and fanout edges $e_i$ and $e_j$
	\end{itemize}
\end{itemize}
\end{frame}

\begin{frame}{Clauses for U-cycles}
\begin{itemize}
\item Pick an arbitrary
direction traversal of u-cycle $C$ of $D$:
	\begin{itemize}
	\item if \alert{at least one} forward edge and at least one backward edge are not selected, then the u-cycle remains d-acyclic after implosion of
the selected edges
	\end{itemize}

\item $f_1, f_2, ... f_k$ adjacency variables for the forward edges of $C$
\item $b_1, b_2, ..., b_l$ for backward edges

\item Clauses for each $C$:
	\begin{itemize}
	\item $\overline{f_1} + \overline{f_2} + \cdot \cdot \cdot + \overline{f_k}$ and $\overline{b_1} + \overline{b_2} + \cdot \cdot \cdot + \overline{b_l}$
	\end{itemize}	
	
\end{itemize}
\end{frame}

\begin{frame}{Self-Loops}
\begin{itemize}
\item If different vertices within a worm depend on each other $\Rightarrow$ self-loops 
\item If \alert{all d-cycles} of a worm-graph $G$ are self-loops $\Rightarrow$ $G$ legal

\item Clauses:
	\begin{itemize}
	\item Require the reconvergent edge $\langle u, v \rangle$ not to be selected
	\end{itemize}	
\end{itemize}
	
\begin{figure}[h]
\centering
\includegraphics[width=0.8\textwidth]{figures/self_loops.png}
\caption{\citet{Liao:1998:NVC:270580.270583}}
\end{figure}	
	
\end{frame}

\begin{frame}{Simple and Composite U-Cycles}
\begin{itemize}
\item Prescribing clauses for all \alert{simple u-cycles is not enough}
\item Find \alert{all u-cycles} in it and prescribe
clauses for each one
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=0.9\textwidth]{figures/simple_composite_cycles.png}
\caption{\citet{Liao:1998:NVC:270580.270583}}
\end{figure}
\end{frame}

\begin{frame}{Clauses for Matches}
\begin{itemize}
\item Commutative operations $\Rightarrow$ \alert{multiple patterns}
\item \alert{Only one match} can be selected
\item $m_1, m_2, ... m_i$ set of matches for vertex $v$
\item Clause for matching on $v$:
	\begin{itemize}
	\item $m_1 + m_2 + ... + m_i$
	\end{itemize}
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=0.85\textwidth]{figures/matches_reload_spills.png}
\caption{\citet{Liao:1998:NVC:270580.270583}}
\end{figure}

\end{frame}

\begin{frame}{Clauses for Reloads and Spills}
\begin{itemize}
\item Source location in either \alert{accumulator or memory}
\item Operand needed by $v$ in the accum. but it's in memory:
	\begin{itemize}
	\item[$\Rightarrow$] Reload operand before scheduling $v$
	\end{itemize}
\item Clauses for $e_1$:
	\begin{itemize}
\item $ m_1 \cdot \overline{e_1} \Rightarrow spill(t) \qquad \qquad \qquad (\equiv \overline{m_1} + e_1 + spill(t)) $
\item $ m_1 \cdot \overline{e_1} \Rightarrow reload(e_1) \qquad \qquad \ \ (\equiv \overline{m_1} + e_1 + reload(e_1)) $
\item $ m_2 \Rightarrow spill(t) \qquad \qquad \qquad \quad \ \ (\equiv \overline{m_2} + spill(t)) $
	\end{itemize}
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=0.65\textwidth]{figures/matches_reload_spills.png}
\caption{\citet{Liao:1998:NVC:270580.270583}}
\end{figure}
\end{frame}

\begin{frame}{Leaves and Roots of the DAG}
\begin{itemize}
\item Leaves are \alert{entry} points $\Rightarrow$ \alert{load} value into accumulator
\item Roots are \alert{exit} points $\Rightarrow$ \alert{store} value to memory 
\item Clause for each edge $\langle u, v \rangle$ where $v$ is a primary output:
	\begin{itemize}
	\item $spill(u)$	
	\end{itemize}
\item Clause for each edge $e = \langle u, v \rangle$ where $u$ is a primary input, if selected match $m$ on $v$ requires:
	\begin{itemize}	
	\item $\overline{m} + reload(e)$
	\end{itemize}
\end{itemize}

\begin{figure}[h]
\centering
\includegraphics[width=0.65\textwidth]{figures/leaves_roots.png}
\caption{\citet{Liao:1998:NVC:270580.270583}}
\end{figure}	
\end{frame}

\begin{frame}{Summary of the Binate Covering Formulation}
\begin{itemize}
\item  Clauses describing the set of all legal worm-partitions:
	\begin{enumerate}
	\item Fundamental adjacency clauses (fanin/fanout)
	\item U-cycle clauses (forward/backward edges)
	\item Clauses for reconvergent edges (self-loops)
	\end{enumerate}
\item Clauses for instruction selection:
	\begin{enumerate}
	\item Implement vertex as a match from a set of alternatives
	\item Relating instruction selection/scheduling to spills/reloads
	\item Primary inputs and primary outputs
	\end{enumerate}
\end{itemize}
\end{frame}

\begin{frame}{Optimality of the Binate Covering Formulation}
\begin{itemize}
\item Optimal solutions for the code generation problem for one-register machines
\item A worm-partition only determines a partial schedule
\item \alert{Any} total schedule derived from the partial schedule has the same optimal cost
\end{itemize}

\begin{figure}
\begin{theorem}
Let $G$ be the worm-partition constructed from an optimal solution of the binate covering problem for one-register machines. Then every total schedule derived from $G$ is optimal.
\end{theorem}
\caption{\citet{Liao:1998:NVC:270580.270583}}
\end{figure}

\end{frame}

\begin{frame}{Summary and ongoing work}
\begin{itemize}
\item Presented a new formulation for the code generation problem for one-register machines
\item The set of all legal worm-partitions can be described by clauses with adjacency variables
\item For multiple-register machines, clauses may be more difficult to write
	\begin{itemize}
	\item[$\Rightarrow$] $Cost(binate\ covering) \overset{?}{>} Cost(generated\ code)$
	\end{itemize}
\item E.g. DSP code gen. with multiple registers not optimal
\end{itemize}
\end{frame}

\begin{frame}[plain,noframenumbering]
	\centering \Large
	\emph{Thanks for your attention!}
	\vskip10pt
	Questions?
\end{frame}

\newpage

\appendix

\bibliographystyle{unsrtnat}
\bibliography{bibliography}

\end{document}
