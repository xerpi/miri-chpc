\documentclass[11pt]{article}
\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage[pdftex]{graphicx}
\usepackage[hidelinks]{hyperref}
\usepackage{url}
\usepackage{csquotes}
\usepackage{tocbibind}
\usepackage[numbers]{natbib}
\usepackage{sectsty}
\usepackage[table]{xcolor}

\newcommand{\horrule}[1]{\rule{\linewidth}{#1}}
%\newcommand{\citeyear}[1]{\citeauthor{#1}~[\citeyear{#1}]}
\newcommand{\note}[1]{ {\small \color{red!80}{\bf Note:} #1 \normalsize} }

\title{
	\vspace{1in} 	
	\horrule{0.5pt} \\[0.4cm]
	\huge Acyclic code scheduling \\
	{\large Compilers for High Performance Computers} \\
	\horrule{2pt} \\
}
\author{
	\Large Sergi Granell Escalfet \\
	\\
	\today
}
\date{}

\begin{document}

\pagenumbering{gobble}

\maketitle

\newpage

\tableofcontents

\newpage

\pagenumbering{arabic}

\iffalse

Introduction

Basic concepts
* Instruction-Level Parallelism
* Code-Scheduling Constraints
  * Control-dependence constraints. 
  * Data-dependence constraints. 
  * Resource constraints.
* Data Dependence
  * True dependence: read after write
  * Antidependence: write after read
  * Output dependence: write after write
  * Data-Dependence Graphs
     DAGs
* Basic-Block Scheduling
  * Basic blocks
  * Control Flow Graph
  * List scheduling
* Global Code Scheduling 
  * More than one basic block at a time

Phase Ordering Between Register Allocation and Code Scheduling, which goes first?

Binate covering, optimal schedule for one-register machines
* Binate covering
* Previous Work
* Worm, worm-partition
* Binate Covering Formulation
* Fundamental Adjacency Clauses
   * If the subject DAG u-acyclic, then the fundamental clauses are sufficient. If u-cycles in D, insufficient.
* Clauses for U-Cycles
  * u-cycles in D may lead to d-cycles in G, we need to add clauses to prevent this from happening
  * For each u-cycle in D, edges of the same orientation may not all be selected.
  * no another cause than u-cycles in D can arise d-cycle in G
* Self-Loops
  * If all d-cycles of a worm-partition G are self-loops, then G is legal. Conversely, if a d-cycle of G contains more than one vertex, then G is illegal.
* Clauses for Matches
* Clauses for Reloads and Spills
* Leaves and Roots of the DAG
* Optimality of the Binate Covering Formulation
  * Let G be the worm-partition constructed from an optimal solution of the binate covering problem for one-register machines. Then every total schedule derived from G is optimal.
* Multiple register machines
  *  For machines with multiple register classes, the clauses may be more difficult to write, and in some cases the cost of a solution to the binate covering problem may not reflect the actual cost of the generated code

Trace Scheduling
* Traces
* Compensation Code
  * Splits
  * Joins
* Instruction Scheduling
* Avoiding Split Copies
* Avoiding Rejoin Compensation
* SUPPRESSING COMPENSATION CODE
  * Ellis’s Algorithm

Performance Evaluation...
\fi

\section{Introduction}

In this report I will start by explaining the basic concepts needed to understand the key aspects of acyclic code scheduling, that is, code without considering loop back edges.

Then I will show how \citet{Liao:1998:NVC:270580.270583} generalize the problem of scheduling instructions on one-register machines in order to apply the binate covering algorithm.

Afterwards I will explain the key aspects behind trace scheduling and the improvements made by \citet{Freudenberger:1994:ASC:183432.183446} over the algorithm proposed by \citet{Ellis:1986:BCV:5910} in order to reduce and avoid compensation code related to the movement of instructions between basic blocks due to trace scheduling.

Finally, I will present the experiments and done by \citet{Bernstein:1992:PEI:144965.145816} and the results obtained regarding some code scheduling optimizations such as basic block scheduling, branch optimizations and invoking a global scheduler.


\section{Basic concepts}

In this section I will explain the basic concepts that serve as the foundation for all the rest of the techniques and algorithms that will be explained throughout the document.

\subsection{Instruction-Level Parallelism}

Instruction-level Parallelism (ILP) is a subset of processor and compiler design techniques that take advantage of the fact that there are many instructions in the code that don't depend on each other in order to execute them in parallel and speed up the execution time.

\subsection{Code-Scheduling Constraints}

Code scheduling is a form of optimization applied by the code generator of the compiler. Its objective is to improve ILP by potentially changing the order of some instructions but always being subject to three constrains \citep{Aho:2006:CPT:1177220}:
\begin{enumerate}
	\item \textit{Control-dependence constraints}: All the operations executed in the original program must be executed in the optimized one.
	\item \textit{Data-dependence constraints}: The operations in the optimized program must produce the same results as the corresponding ones in the original program.
	\item \textit{Resource constraints}: The schedule must not oversubscribe the resources on the machine.
\end{enumerate}

\subsection{Data Dependencies}

A data dependency occurs when an instructions refers to the data of a preceding instruction. There are three types of dependencies depending on the kind depending on the kind (read or write) of data references:

\begin{enumerate}
	\item \textit{True dependence}: read after write (RAW).
	\item \textit{Antidependence}: write after read (WAR).
	\item \textit{Output dependence}: write after write (WAW).
\end{enumerate}

\subsubsection{Data-Dependence Graphs}

The instructions of the code and their dependencies can be represented as directed graphs, and in particular a directed acyclic graph (DAG) when dependencies carried by loops are left out.

\subsection{Basic-Block Scheduling}

\subsubsection{Basic blocks}

A basic block (BB) is a sequence of instructions with no control enters or exits in the middle except to the entry branch and the exit branch. In other words, a sequence of instructions is a basic block if: a) each instruction dominates the succeeding instructions, and b) no other instruction executes between two instructions in the sequence.

\subsubsection{Control Flow Graph}

Since any instruction that is the target of a branch or that immediately follows a branch starts a BB, a DAG called control flow graph (CFG) can be constructed, where the nodes are the BBs and the edges are the branches between BBs.

\subsubsection{List scheduling}

List scheduling is a kind of topological algorithm that builds an schedule by taking as input the DAG of the instructions to schedule and resources they take, such as the functional units of the CPU and their duration in cycles.

It works by keeping a ready list containing all the instructions with satisfied dependencies and by filling in the schedule cycle-by-cycle by taking ready instructions with their corresponding processor resources available (reservation tables). Since this problem is known to be NP-complete \cite{Coffman-NP}, some heuristics such as height-based priority can be applied.

\subsection{Global Code Scheduling}

Since the average size of a basic block is quite small (a dozen of instructions), the effectiveness of instruction scheduling in BBs is limited. The idea behind global scheduling algorithms is grouping a sequence of basic blocks and schedule their instructions as if they were a single bigger BB.
Depending on the characteristics, such groupings have different names:
\begin{itemize}
	\item \textit{Trace}: a frequently executed acyclic sequence of BBs in a CFG.
	\item \textit{Superblocks}: a trace without side entrances (single-entry multiple-exit).
	\item \textit{Hyperblocks}: single-entry multiple-exit traces with internal control flow effectuated via instruction predication. If-conversion folds conditional branches into predicated instructions.
	\item \textit{Treegion}: trees of blocks such that no block in a treegion has side entrances. Any path through a treegion is a superblock.
\end{itemize}

\section{Phase Ordering Between Register Allocation and Code Scheduling}

Register allocation and code scheduling are among the last steps of the compilation process (code generation). Depending on the kind of processor (pipelined, multiple-issue, etc.) the compiler is targeting it might be better to to execute one of these phases before the other one. It is also possible to perform scheduling both before and after register allocation.

Performing register allocation first makes sure that code that requires more registers than are available isn't generated. The drawback of performing scheduling first, oblivious of the register allocation, is that shorter schedules tend to yield greater register pressure. In the event that a viable allocation cannot be found, spill code must be inserted \cite{Rau:1993:IPP:172782.172784}.

\section{Binate covering, scheduling one-register machines}

In this section I will explain how sometimes a problem can be solved by generalizing it and adapting it to an already known algorithm, in particular adapting the problem of scheduling instructions of one-register machines into the binate covering problem.

\subsection{Binate covering}

An instance of the \textit{binate covering problem} is defined as a triple $\langle X, Y, cost \rangle$ where $X$ is a set of Boolean variables, $Y$ is a set of clauses, and $cost$ is a cost function that maps $X$ to the non-negative integers. Each clause $y_i \in Y$ is a disjunction of variables (either true or complemented) of $X$.

The objective is to assign each $x_j \in X$ a value of 0 or 1 such that every $y_i$ is evaluated to 1 and such that the total cost of the variables that are set to 1 (that is $\sum_{x_j: x_j = 1} cost(x_j)$) is minimized.

\subsection{Previous Work}

Even though \citet{Aho:1977:CGE:321992.322001} presented optimal code generation algorithms (on DAGs) for one-register machines, their model is not sufficiently expressive to take into account the availability and costs of commutative forms of certain operators. \citet{Liao:1998:NVC:270580.270583} believe it's better to handle commutativity by using a separate pattern for each of the commutative forms of the operations.

\subsection{Worms, worm-partitions}

We will consider a subject DAG as DAG without its inputs and outputs vertices and their connected edges. A DAG can be \textit{d-cyclic} or \textit{d-acyclic} depending on whether it has cycles, or  \textit{u-cyclic}/\textit{u-acyclic} when considering undirected edges.

Let a subject DAG $D \langle V, E \rangle$ be given. A \textit{worm} $w$ in $D$ is a subset of $V \cup E$ forming a directed path, possibly of zero length, such that the vertices in the path will appear consecutively in the schedule, a \textit{worm-partition} of $D$ is a set of disjoint worms, and a \textit{worm-graph} $G$ of $D$ is a directed \textit{multigraph} with each \textit{worm-partition} collapsed into single vertex (\citet{Aho:1977:CGE:321992.322001}).

Since every d-acyclic DAG has a topological sort, a \textit{sufficient}, but not always necessary condition for a worm-graph to be \textit{legal} (if a valid schedule can be derived from) is that G be d-acyclic (\citet{Aho:1977:CGE:321992.322001}).

\subsection{Binate Covering Formulation}

We can map the binate covering problem in one-register machines by having the following sets of Boolean variables: (1) set of \textit{adjacency variables}, each of them associated with each edge $e_i$, and (2) set composed of the variables for matches $m$ of patters at vertex $v$, spills (vertex $v$ has to be spilled into the memory), and reloads (value transmitted by edge $e$ needs to be reloaded from the memory before the operation that uses $e$ is executed).

\subsubsection{Fundamental Adjacency Clauses}

The \textit{fundamental adjacency constraints} states that if a vertex has multiple fanouts, then at most one of the fanout edges may be selected. If a vertex has multiple fanins, then at most one of the fanin edges may be selected. Those constraints are necessary for a worm-graph to be legal in any DAG, and are actually sufficient if the subject DAG is u-acyclic (\citet{Liao:1998:NVC:270580.270583}).

The clauses for the fundamental constraints are $\overline{e_i} + \overline{e_j}$ for every pair of fanin and fanout edges $e_i$ and $e_j$.

\subsubsection{Clauses for U-cycles}

As demonstrated by \citet{Liao:1998:NVC:270580.270583}, given a u-cycle $C$ of D and an arbitrary direction traversal of $C$, if at least one forward edge and at least one backward edge are not selected, then the u-cycle remains d-acyclic after implosion of the selected edges.
In other words, to avoid having d-cycles in $G$, for each u-cycle in $D$, edges of the same orientation
should not all be selected.

Let $f_1, f_2, ... f_k$ be the adjacency variables for the forward edges of a u-cycle in $D$, and $b_1, b_2, ..., b_l$ for the backward edges, for each u-cycle we can write to clauses to ensure that not all of the forward edges and not all of the backward edges are selected: $\overline{f_1} + \overline{f_2} + \cdot \cdot \cdot + \overline{f_k}$ and $\overline{b_1} + \overline{b_2} + \cdot \cdot \cdot + \overline{b_l}$.

\subsubsection{Self-Loops}

If different vertices within a worm depend on each other, this may lead to self-loops in $G$. \citet{Liao:1998:NVC:270580.270583} prove that if all d-cycles of a worm-graph $G$ are self-loops, then $G$ is legal. In other words, if a d-cycle of $G$ contains more than one vertex, then $G$ is illegal.

For self-loops, the two clauses related to forward and backward edges selection are not needed, Instead, a clause consisting of a single variable requiring the reconvergent edge $\langle u, v \rangle$ not to be selected is enough.

\subsubsection{Simple and Composite U-Cycles}

Even though one might think that if the u-cycle constraints are satisfied for all simple u-cycles, then they would be satisfied for all composite u-cycles, that's not the case and given a DAG we have to find all u-cycles in it and prescribe
clauses for each one.

It's actually possible to \enquote{add} the set of cycles since the sum of two u-cycles may not always be two u-cycles, and therefore reducing their number.

\subsubsection{Clauses for Matches}

Given that every vertex in the DAG has to be implemented by some pattern, there might be a set of alternatives for commutative operations. Let $m_1, m_2, ... m_i$ be the set of matches for a given vertex $v$, then the matching clause for $v$ would be: $m_1 + m_2 + ... + m_i$, since only one match has the be selected.

\subsubsection{Clauses for Reloads and Spills}

Depending on the source operands (accumulator or memory) of the operation and the source of the selected edge, it might be needed to perform reloads and spills.

Let $m_1$ be a match that takes the accumulator on the left (edge $e_1$) and memory on the right (edge $e_2$), and $m_2$ a match with reversed inputs. In case both $m_1$ and $e_1$ are selected, there won't be any need for spills or reloads since the operands' location match. On the other case, if for example $m_1$ and $e_2$ are selected, the value ($t$) coming from $e_1$ will have to be spilled, and then reloaded just before the execution of the vertex that $m_1$ matched. If $m_2$ is taken, a spill of $t$ will have to be performed. In this example we would have three clauses for $e_1$:
\[ m_1 \cdot \overline{e_1} \Rightarrow spill(t) 	\equiv \overline{m_1} + e_1 + spill(t)\]
\[ m_1 \cdot \overline{e_1} \Rightarrow reload(e_1) 	\equiv \overline{m_1} + e_1 + reload(e_1)\]
\[ m_2 \Rightarrow spill(t) 	\equiv \overline{m_2} + spill(t)\]

Therefore, one can build the clauses for reloads and spills for every vertex.

\subsubsection{Leaves and Roots of the DAG}

For leaves (entry points), their values must be loaded into the accumulator, and for roots (exit points), their values have to be stored into memory.
Therefore, for each edge $\langle u, v \rangle$ such as that $v$ is a primary output, we require the following clause: $spill(u)$. Whereas if $u$ is a primary input, we need to reload its dependent variable over the edge $e$ if the selected match $m$ so requires: $\overline{m_1} + reload(e)$.

\subsubsection{Optimality of the Binate Covering Formulation}

\citet{Liao:1998:NVC:270580.270583} prove that the presented binary covering formulation yields to an optimal solutions for the code generation problem for one-register machines by showing that even though a worm-graph only determines a partial schedule, any total schedule derived from the partial schedule has the same optimal cost.

\subsection{Experiments and results}

\citet{Liao:1998:NVC:270580.270583} implemented the explained algorithm on the code generator of a simplified TMS320C25 [Texas Instruments 1993] architecture. They claim to have achieved up to 59\% reduction in the optimizable costs, which translates to up to 27\% reduction in overall code size.

\subsection{Future work for multiple register machines}

The presented clauses with adjacency variables can express the set of all legal worm-graphs on one-register machines. For multiple-register machines, the clauses may be more difficult to write, and in some cases the cost of a solution to the binate covering problem may not reflect the actual
cost of the generated code (\citet{Liao:1998:NVC:270580.270583}).

\section{Trace Scheduling}

In this section I will explain the process \citet{Freudenberger:1994:ASC:183432.183446} went in order to improve the algorithm proposed by \citet{Ellis:1986:BCV:5910} in order to reduce and avoid compensation code related to the movement of instructions between basic blocks due to trace scheduling.

\subsection{Traces}

As explained before, traces are a sequence of basic blocks, which can be used to schedule all of its instructions together and therefore giving the scheduler more opportunity for better ILP.

\subsection{Compensation Code}

When an operation is moved across basic block boundaries, one or more compensation copies may be required in the off-trace code. For example, if an instruction that appeared above a branch is moved below it, then the code outside of the trace must be fixed up by inserting a compensation copy into the off-trace code.

Although this added code due to the compensation increases the size of the program generated, \citet{Freudenberger:1994:ASC:183432.183446} show how at the end this is not a practical concern. Their study reveals two key results: the amount of compensation code generated is not large and compensation code is not critical to obtain the benefits of trace scheduling.

In order to limit the amount of compensation code two techniques are proposed: avoidance (restricting code motion so that no compensation code is required) and suppression (analyzing the global flow of the program to detect when a copy is redundant).

\subsubsection{Traces and schedules}

The trace scheduler takes a trace, that is, a sequence of operations, and generates a sequence of instructions into what's called the schedule. The trace most likely has been subject to a set of optimizations. Logically, we can divide this work into two parts: (1) the trace scheduler (TS) which selects the sequence of BBs and passes it to (2) the instruction scheduler (IS), which given the trace, allocates machine resources and registers to produce the schedule. The TS then updates the trace with the scheduled part to add compensation code (\textit{bookkeeping}), if necessary, and handles the trace back to the IS.

Each operation $O$ of a trace has an unique position, $TracePosition(O)$. Instructions in the schedule produced by the IS are also ordered, with \break $FirstCycle(O)$ and $LastCycle(O)$ as first and last cycles in the schedule, respectively. The instantiation of $O$ on the schedule is $scheduled(O)$, and the compensation copy of $O$ is $copied(O)$, also denotes as $O'$ or $O''$.

\subsubsection{Splits and Joins}

Each BB in the code is determined by either branches (splits) and branch targets (joins). Whenever an operation crosses a BB boundary, fix up copies will have to be generated.

\paragraph{Splits}

A split is an operation with more than one successor (on-trace and off-trace), usually result of conditional or indirect branches.

When the IS moves an operation below the split on the schedule, the TS must copy that operation to the off-trace edge. A pseudo-op $SP$ can be associated with each split edge, which represents the the first successor of the off-trace.

Given a split $S$ we can determine a tuple of compensation copies \break $(O'_1, ..., O'_m)$ where $TracePosition(O_i) < TracePosition(S)$ and \break $FirstCycle(O_i) > LastCycle(S)$.

\paragraph{Joins}

Joins are branch operations that target the trace. For each join $J$, we associate a pseudo-op $RP$ that is the immediate off-trace predecessor.

When the TS moves an operation above a join, it must insert a copy of this operation on the off-trace joining edge, in source order between RP and the schedule.

The trace scheduler has to find an appropriate instruction that can serve as the branch target, since just choosing the instruction that contains the joined operation is not always correct. Therefore, the trace scheduler must find for each join $J$ on the trace a rejoin instruction $RI$ such as that if a branch operation targeted the join $J$, the corresponding branch instruction targets instruction $RI$.

The $RI$ of join $J$ has to satisfy the constraint that all operations $O$ with $TracePosition(O) < TracePosition(J)$ must be scheduled before $RI$.

Once $RI$ is determined, the TS can determine the join compensation code for each join $J$ with rejoin instruction $RI$ as a tuple of compensation copies $(O'_1, ..., O'_m)$ where $TracePosition(O_i) \geq TracePosition(J)$ and $FirstCycle(O_i) < RI$.

\paragraph{Copied Split Operations}

If a split is copied into a joining edge, all operations that are between the join and the split on the trace and that are not before the rejoin instruction in the schedule must be copied onto the off-trace edge of the copied split.

For each split $SJ$ copied on an edge joining the trace at \break join $J$ with rejoin instruction $RI$, there exists a tuple of compensation \break code $(O'_1, ..., O'_m)$, where $TracePosition(O_i) \geq TracePosition(J)$, \break $TracePosition(O_i) < TracePosition(SJ)$, and $FirstCycle(O_i) \geq RI$.

\paragraph{Speculative Code Motion}

Moving an operation that was below a split on the trace to above it on the schedule doesn't produce compensation code, and can be fixed by for example targeting other registers that are not live on the off-trace paths.

\subsection{Instruction Scheduling}

The job of the instruction scheduler is to generate a schedule that optimizes the performance on the target hardware. Since data dependencies have to be respected, each split edge can be associated with variables that are live on the off-trace path, which the TS will update after each trace is scheduled.

Register allocation is performed incrementally as the compiler schedules each trace.

\subsection{Avoiding Compensation Code}

To limit the amount of compensation code, the compiler used by \citet{Freudenberger:1994:ASC:183432.183446} places a number of restrictions.

\subsubsection{Avoiding Split Copies}

In order to limit split compensation, the TS requires all the operations that precede a split on the trace to also precede a split on the schedule, which as a side effect requires all splits to be scheduled in source order, although this restriction limits the ILP available to the schedule, and therefore effecting the performance.

\subsubsection{Avoiding Rejoin Compensation}

Since a joined operation can sometimes serve as the target of multiple branch operations, the decision of having separate compensation code for each joining edge or having a single instance of the compensation codes has to be done.

On one hand, when choosing separate copies, a single branch instruction is needed to the $RI$, avoiding jumps to jumps. On the other hand having a single copy of compensation code reduces the amount of duplication and therefore the code growth.

The heuristic opted by \citet{Freudenberger:1994:ASC:183432.183446} is to have the code copies separated for each joining edge, but limiting code motion above an operation that has more than 4 predecessors.

\subsubsection{Loops}

A trace does not take into account the back edge of loops (acyclic code scheduling), therefore the only way a loop can expose its ILP to the trace scheduler, other than using cyclic scheduling techniques such as module scheduling, is by unrolling its iterations. Once the loop body has been unrolled, speculative code motion can be relied on in order to overlap the code and expose more ILP.

Actually, the motivation of \citet{Freudenberger:1994:ASC:183432.183446} for compensation copy suppression was to reduce the large amount of join compensation codes of loops with internal branches, such as having \textit{if} conditionals inside loops, which allowed for higher levels of loop unrolling.

\subsubsection{Fail-Safe Trace Scheduling Shutdown}

When the number of copies of code due to compensation grow to much (twice the size for \citet{Freudenberger:1994:ASC:183432.183446}), the TS can be stopped from generating more compensation code. This can help the reduce the program compilation time, as can be considered as a fail-safe recovery from worst-case copying (such as high degree level of unrolling of a loop with internal branches). They also explain that fail-safe shutdown in vaguely needed in most programs.

\subsection{Suppressing Compensation Code}

There are many reasons that makes introduction of compensation code undesirable. Mainly, when we add copies of operations the resulting program size and its compilation time increase, and therefore it has a big impact on the instruction cache and the final performance. Not only that but adding the compensation code, which sometimes might be redundant, to the off-trace can cause the off-trace code to not be scheduled as efficiently as it would without such new code.

Therefore, given $O'$ as a copy of operation $O$, $O'$ can be said to be redundant and can be suppressed if these two conditions are satisfied:
\begin{enumerate}
	\item $Scheduled(O)$ (or another $copied(O)$) dominates (is executed before) any path that includes the location where $O'$ is to be placed.
	\item The dominating operation has the same effect as the copy $O'$.
\end{enumerate}

The following two sections present the exact conditions needed to satisfy these requirements.

\paragraph{Compensation Code Optimization Problem}

Given a trace $T$, a join $J$, an operation $O$ such that $TracePosition(O) > TracePosition(J)$, and a schedule $TS(T)$ for the trace $T$ with rejoin instruction $RI$ for $J$ such that $FirstCycle(O) <
RI$, determine if the copy $O'$ is redundant, and remove the copy if this is the case.

\paragraph{Split Compensation Code Optimization Problem}

Given a trace $T$, a split operation $S$, an operation $O$ such that $TracePositon(O) <$\break$ TracePosition(S)$, and a schedule $TS(T)$ for the trace $T$ such that\break$LastCycle(S) < FirstCycle(O)$, determine if the copy $O'$ is redundant, and remove the copy if this is the case.

Given the definition of rejoin instruction presented here (which is the same as the one presented by \citep{Freudenberger:1994:ASC:183432.183446}), it will never be possible for any split compensation copies to be suppressed. It is possible, however, to identify and remove redundant split compensation copies by adjusting the rejoin to the schedule not to branch around operations that originally dominated the rejoin on the trace. They can also be avoided by restricting code motion below splits.

\subsubsection{Ellis’s Algorithm}

\citet{Ellis:1986:BCV:5910} proposed an algorithm for detecting redundant copies after describing the problem of redundant join compensation code.

Their proposed algorithm is the following one: let $RP$ be the pseudo-op for a rejoining edge $J$ and let $O$ be an operation that follows $J$ on the trace ($TracePosition(O) > TracePosition(J)$). Let the rejoin instruction for this join be $RI$, and let $FirstCycle(O) < RI$. Then $O$ will be copied onto this rejoin edge. The rejoin copy $O'$ is redundant if, after scheduling this trace:
\begin{itemize}
	\item $Scheduled(O)$ dominates $RP$. In other words, every path from program entry to $RP$ first executes $Scheduled(O)$.
	\item $Scheduled(O)$ copy-reaches $RP$. In other words, on each path from $Scheduled(O)$ to $RP$, none of the input or output variables of $O$ are assigned between the last execution of $Scheduled(O)$ and $RP$.
\end{itemize}

\citet{Freudenberger:1994:ASC:183432.183446} extended Ellis' algorithm from the following directions:
\begin{itemize}
	\item Ellis' dominance requirement was too restrictive in the cases that mattered the most for performance: unrolled loops with internal branches. The dominance requirement was changed to be either $Scheduled(O)$ or some other copy of $O$ be executed on every path to $RP$.
	\item They developed an algorithm that detects copies after register allocation for the schedule, whereas Ellis' algorithm could only detect most redundant copies with a maximally renamed program representation due to the antidependencies and output dependencies between variables introduced during register allocation.
	\item Their algorithm works incrementally, that is, it doesn't require an analysis of the full program between the scheduling of each trace.
\end{itemize}

\subsection{Evaluation}

\citep{Freudenberger:1994:ASC:183432.183446} evaluated the effectiveness of trace scheduling and their copy compensation code optimizations by running a series of experiments: compiling and running the SPEC89
benchmarks (\cite{DIXIT19911195}) on a compiler implementing the said optimizations. Their experiments indicate that:
\begin{itemize}
	\item Trace scheduling gives significant performance improvements over the compilation techniques used for most RISC processors. Much of this performance gain can be achieved without requiring any compensation code.
	\item The code growth due to trace scheduling and the loop unrolling used to support it is small, and comparable to the code growth due to the loop unrolling and pre- or postconditioning done by most RISC compilers.
	\item The amount of code growth due to compensation code is very small. Thanks to their techniques Split compensation code can be avoided successfully with a minimal performance penalty. On the other side, avoiding join compensation code is more difficult, and their copy suppression algorithm is only partially successful.
\end{itemize}

\section{Performance evaluation of instruction scheduling on the IBM RISC System/6000}

\citet{Bernstein:1992:PEI:144965.145816} evaluated a set of of instruction scheduling techniques which were implemented in the IBM XL compiler to fully exploit the parallel/pipelined features of the IBM RISC System/6000 superscalar processor, capable of executing fixed point and floating point instructions in parallel.

\subsection{Machine model}

For their machine model, they viewed the RISC superscalar machine as a collection of units of $m$ types, with $n_1, n_2, ..., n_m$ units of each type, with all the computations done in registers and only load a store instructions being able to reference memory.

The RS/6K processor is for instance modeled with $m = 3$ functional units (one fixed point, one floating point and one branch unit). Most of the instructions are executed in one cycle and five main types of pipeline delays: at least one cycle between loads and the instruction that uses the result, one cycle between depending floating point instructions, three cycles between a fixed point compare and the depending branch instruction (CR field), five cycles between a floating point compare and the depending branch instruction (CR field) and two cycles between consecutive branches, where the first one is conditional.

\subsection{Scheduling Optimizations}

They consider a vast number of compiler and scheduler optimizations:
\begin{enumerate}
	\item First a set of traditional machine-independent optimizations (such as value numbering, common subexpression elimination, constant propagation, etc.) is applied.
	\item After this phase, the machine-dependent scheduling techniques are employed on an intermediate code representation which uses (an unlimited number of) symbolic registers, like static single assignment (SSA). Global scheduling is applied first, followed by a first pass of basic block scheduling, which is aimed at fine-tuning the code produced by global scheduling.
	\item Afterwards, register allocation takes place, followed by an additional pass of basic block scheduling, this time on real registers.
	\item Finally, branch optimizations try to improve the code where other optimizations have failed. 
\end{enumerate}

\subsection{Performance Results}

The results were obtained using a 42 MHz model 550 HS/6K machine, the AIX 3.1 operating system, and a prototype version of the XL C and Fortran compilers.

Ten benchmarks of the SPEC-89 suite \cite{DIXIT19911195} were evaluated, of which four are integer programs, while the rest mainly include floating point computations.

The results are given in a relative form, describing the change in percents upon invoking the optimizations being discussed, as compared to the corresponding base result, which is the performance of the compiled code with all the scheduling optimizations disabled. In the following tables, BB stands for invoking only the basic block scheduler, BO for invoking only the branch optimizations (code replication, gluing, and branch swapping), GL for invoking only the global scheduler, BBO for invoking the basic block scheduler and branch optimizations, and ALL for invoking all of them together.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{spec_results1.png}
\caption{SPEC suite results depending on optimizations. By \citet{Bernstein:1992:PEI:144965.145816}.}
\label{spec-res-1}
\end{figure}

As seen on Figure \ref{spec-res-1}, while the combined improvement of all the programs is 18\%, the integer benchmarks are improved by 22\%, and the floating point benchmarks are improved by only 15\%. Since the basic block scheduler affects the integer and floating point benchmarks almost equally, this difference is definitely due to global scheduling and, most importantly, by the branch optimizations. 

RS/6K branching delays are crucial for the overall performance for integer programs, which typically have complex control flow graphs. This can be seen in the \textit{BO} column of Table 1 of Figure \ref{spec-res-1}, where for integer benchmarks the improvement is 11\%.

For floating point benchmarks, typically featuring large basic blocks and relatively simple control flow graphs, branching performance is of a lesser importance. Indeed the branch optimizations offer a smaller improvement in that case (3\%). Summing the individual improvements for floating point benchmarks, clearly points to an overlap between the impact of basic block and global schedulers. On the other hand, for both types of the benchmarks, by examining the \textit{BBO} column in Table 1 of Figure \ref{spec-res-1}, we can notice that there is almost no overlap between the basic block scheduler and the branch optimizations. 

The compile time cost are summarized in Table 2 of Figure \ref{spec-res-1} (the \textit{BBO} column which is just the sum of the \textit{BB} and \textit{BO} columns is omitted. The branch optimizations are of the lowest cost (1\%), and the global scheduling is the most expensive (12\%).
Also notice how time to perform all optimizations (\textit{ALL}) exceeds the sum of the times to perform them individually. This can be explained because global scheduling, code replication and gluing all increase code size, therefore increasing compile time in other scheduling optimizations. 

\newpage

\bibliographystyle{unsrtnat}
\bibliography{bibliography}

\end{document}